package com.example.crypto.utils;

import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;

public class Constant {

    private Constant() {
    }

    public static BigDecimal ONE_HOUNDRED = new BigDecimal(100);
    public static final CurrencyPair CURRENCY = CurrencyPair.BTC_USD;
}
