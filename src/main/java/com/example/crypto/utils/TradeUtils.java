package com.example.crypto.utils;

import com.example.crypto.exception.ItemNotFound;
import org.knowm.xchange.dto.marketdata.Trade;

import java.math.BigDecimal;
import java.util.List;

import static com.example.crypto.utils.Constant.ONE_HOUNDRED;
import static java.util.Comparator.naturalOrder;

public class TradeUtils {

    private TradeUtils() {
    }

    public static String getVolatility(BigDecimal min, BigDecimal max) {
        return (max.multiply(ONE_HOUNDRED).divide(min, 10, BigDecimal.ROUND_HALF_UP)).subtract(ONE_HOUNDRED).toString();
    }

    public static String getTradesValotility(List<Trade> trades) {
        BigDecimal max = trades.stream().map(Trade::getPrice).max(naturalOrder())
                .orElseThrow(ItemNotFound::new);

        BigDecimal min = trades.stream().map(Trade::getPrice).min(naturalOrder())
                .orElseThrow(ItemNotFound::new);

        return getVolatility(max, min);
    }
}
