package com.example.crypto.mapper;

import com.example.crypto.dto.*;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trade;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.crypto.utils.TradeUtils.getTradesValotility;
import static com.example.crypto.utils.TradeUtils.getVolatility;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Component
public class ChartDataMapper {

    public OrderBookDto mapOrderBook(Optional<OrderBook> orderBookOptional, String currency) {
        OrderBookDto orderBookDto = new OrderBookDto();

        if (!orderBookOptional.isPresent()) {
            return orderBookDto;
        }

        OrderBook orderBook = orderBookOptional.get();

        orderBookDto.setDate(orderBook.getTimeStamp().toString());

        orderBookDto.setCurrency(currency);

        List<LimitOrderDto> bids = orderBook.getBids().stream()
                .map(this::mapLimitOrder).collect(Collectors.toList());

        List<LimitOrderDto> asks = orderBook.getAsks().stream()
                .map(this::mapLimitOrder).collect(Collectors.toList());

        orderBookDto.setBids(bids);
        orderBookDto.setAsks(asks);
        return orderBookDto;
    }

    public TradingDataDto mapTradingData(Optional<Ticker> tickerOptional, Optional<Trades> tradesOptional, String currency) {
        TradingDataDto tradingDataDto = new TradingDataDto();

        tradingDataDto.setCurrency(currency);

        tickerOptional.ifPresent(i -> {
            Ticker ticker = tickerOptional.get();
            tradingDataDto.setVolatilityDay(getVolatility(ticker.getLow(), ticker.getHigh()));
            tradingDataDto.setVolume(ticker.getVolume().toString());
        });

        tradesOptional.filter(i -> i.getTrades() != null && !i.getTrades().isEmpty())
                .map(i -> i.getTrades().stream().collect(groupingBy(d -> d.getTimestamp().getHours())))
                .ifPresent(groups -> tradingDataDto.setTradingHourlyData(mapTradesGroup(groups)));

        return tradingDataDto;
    }

    private LimitOrderDto mapLimitOrder(LimitOrder limitOrder) {
        LimitOrderDto limitOrderDto = new LimitOrderDto();

        limitOrderDto.setPrice(limitOrder.getLimitPrice().toString());
        limitOrderDto.setAmount(limitOrder.getOriginalAmount().toString());
        return limitOrderDto;
    }

    private List<TradingHourlyDataDto> mapTradesGroup(Map<Integer, List<Trade>> trades) {
        return trades.entrySet().stream().map(i -> {

            TradingHourlyDataDto tradingHourlyDataDto = new TradingHourlyDataDto();
            tradingHourlyDataDto.setHours(String.format("%d - %d", i.getKey(), i.getKey() + 1));
            tradingHourlyDataDto.setVolatility(getTradesValotility(i.getValue()));
            return tradingHourlyDataDto;
        }).collect(toList());
    }
}
