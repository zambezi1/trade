package com.example.crypto.exception;

public class ItemNotFound extends RuntimeException {

    public ItemNotFound() {
    }

    public ItemNotFound(String message) {
        super(message);
    }

    public ItemNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemNotFound(Throwable cause) {
        super(cause);
    }

    public ItemNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
