package com.example.crypto.facade;

import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;

public interface ChartFacade {
    OrderBookDto getCharsData();

    TradingDataDto getTradingDataData();
}
