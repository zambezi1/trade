package com.example.crypto.facade;

import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;
import com.example.crypto.mapper.ChartDataMapper;
import com.example.crypto.service.ChartService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.example.crypto.utils.Constant.CURRENCY;


@Service
public class ChartFacadeImpl implements ChartFacade {

    @Resource
    private ChartService charService;

    @Resource
    private ChartDataMapper chartDataMapper;

    @Override
    public OrderBookDto getCharsData() {
        return chartDataMapper.mapOrderBook(charService.getOrderBook(CURRENCY), CURRENCY.toString());
    }

    @Override
    public TradingDataDto getTradingDataData() {
        return chartDataMapper.mapTradingData(charService.getTicker(CURRENCY), charService.getTradesForDay(CURRENCY), CURRENCY.toString());
    }
}
