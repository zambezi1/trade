package com.example.crypto.dto;

import java.util.List;

public class TradingDataDto {
    private String currency;
    private String volume;
    private String volatilityDay;
    private List<TradingHourlyDataDto> tradingHourlyData;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getVolatilityDay() {
        return volatilityDay;
    }

    public void setVolatilityDay(String volatilityDay) {
        this.volatilityDay = volatilityDay;
    }

    public List<TradingHourlyDataDto> getTradingHourlyData() {
        return tradingHourlyData;
    }

    public void setTradingHourlyData(List<TradingHourlyDataDto> tradingHourlyData) {
        this.tradingHourlyData = tradingHourlyData;
    }
}
