package com.example.crypto.dto;

import java.util.List;

public class TradingHourlyDataDto {
    private String hours;
    private String volatility;

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getVolatility() {
        return volatility;
    }

    public void setVolatility(String volatility) {
        this.volatility = volatility;
    }
}
