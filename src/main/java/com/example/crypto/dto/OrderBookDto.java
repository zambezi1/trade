package com.example.crypto.dto;

import java.util.List;

public class OrderBookDto {
    private String currency;
    private String date;
    private List<LimitOrderDto> asks;
    private List<LimitOrderDto> bids;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<LimitOrderDto> getAsks() {
        return asks;
    }

    public void setAsks(List<LimitOrderDto> asks) {
        this.asks = asks;
    }

    public List<LimitOrderDto> getBids() {
        return bids;
    }

    public void setBids(List<LimitOrderDto> bids) {
        this.bids = bids;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}