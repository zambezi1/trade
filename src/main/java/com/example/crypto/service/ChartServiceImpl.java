package com.example.crypto.service;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bitstamp.BitstampExchange;
import org.knowm.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class ChartServiceImpl implements ChartService {

    private static final Logger LOG = LoggerFactory.getLogger(ChartServiceImpl.class);

    private Exchange bitstampExchange;

    {
        bitstampExchange = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName());

    }

    public Optional<OrderBook> getOrderBook(CurrencyPair currencyPair) {
        try {
            return Optional.of(bitstampExchange.getMarketDataService().getOrderBook(currencyPair));
        } catch (IOException e) {
            LOG.error("OrderBook problem", e);
            return Optional.empty();
        }
    }

    public Optional<Ticker> getTicker(CurrencyPair currencyPair) {
        try {
            return Optional.of(bitstampExchange.getMarketDataService().getTicker(currencyPair));
        } catch (IOException e) {
            LOG.error("Ticker problem", e);
            return Optional.empty();
        }
    }

    public Optional<Trades> getTradesForDay(CurrencyPair currencyPair) {
        try {
            return Optional.of(bitstampExchange.getMarketDataService().getTrades(currencyPair, BitstampMarketDataServiceRaw.BitstampTime.DAY));
        } catch (IOException e) {
            LOG.error("Trades problem", e);
            return Optional.empty();
        }
    }
}
