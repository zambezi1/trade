package com.example.crypto.service;

import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades;

import java.util.Optional;

public interface ChartService {
    Optional<OrderBook> getOrderBook(CurrencyPair currencyPair);

    Optional<Ticker> getTicker(CurrencyPair currencyPair);

    Optional<Trades> getTradesForDay(CurrencyPair currencyPair);
}
