package com.example.crypto.controller;

import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;
import com.example.crypto.facade.ChartFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ChartController {

    @Resource
    private ChartFacade chartFacade;

    @GetMapping(value = "/orderbook")
    public OrderBookDto getOrderBookData() {
        return chartFacade.getCharsData();
    }

    @GetMapping(value = "/trading")
    public TradingDataDto getTradingData() {
        return chartFacade.getTradingDataData();
    }
}
