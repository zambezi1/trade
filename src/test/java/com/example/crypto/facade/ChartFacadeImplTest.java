package com.example.crypto.facade;

import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;
import com.example.crypto.mapper.ChartDataMapper;
import com.example.crypto.service.ChartService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.annotation.Resource;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.example.crypto.utils.Constant.CURRENCY;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChartFacadeImplTest {

    @Mock
    private ChartService charService;

    @Mock
    private ChartDataMapper chartDataMapper;

    @Mock
    private OrderBookDto orderBookDto;

    @Mock
    private TradingDataDto tradingDataDto;

    private OrderBook orderBook;

    private Ticker ticker;

    private Trades trades;

    @InjectMocks
    private ChartFacadeImpl chartFacade;

    @Before
    public void init() {
        orderBook = new OrderBook(new Date(), Collections.emptyList(), Collections.emptyList());
        ticker = new Ticker.Builder().build();
        trades = new Trades(Collections.emptyList());
    }

    @Test
    public void getCharsDataTest() {
        Optional<OrderBook> orderBookOptional = Optional.of(orderBook);
        when(charService.getOrderBook(CURRENCY)).thenReturn(orderBookOptional);
        when(chartDataMapper.mapOrderBook(orderBookOptional, CURRENCY.toString())).thenReturn(orderBookDto);
        OrderBookDto orderBookData = chartFacade.getCharsData();
        assertEquals(orderBookDto, orderBookData);
    }

    @Test
    public void getTradingDataDataTest() {
        Optional<Ticker> tickerOptional = Optional.of(ticker);
        when(charService.getTicker(CURRENCY)).thenReturn(tickerOptional);

        Optional<Trades> tradesOptional = Optional.of(trades);
        when(charService.getTradesForDay(CURRENCY)).thenReturn(tradesOptional);

        when(chartDataMapper.mapTradingData(tickerOptional, tradesOptional, CURRENCY.toString())).thenReturn(tradingDataDto);
        TradingDataDto tradingDataData = chartFacade.getTradingDataData();
        assertEquals(tradingDataDto, tradingDataData);
    }
}