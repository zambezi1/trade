package com.example.crypto.controller;

import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ChartController.class)
public class ChartControllerTest {

    private static final String TEST_CURRENCY = "TEST_C";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ChartController chartController;

    @Test
    public void getChartDataTest() throws Exception {
        OrderBookDto orderBookDto = new OrderBookDto();
        orderBookDto.setCurrency(TEST_CURRENCY);
        given(chartController.getOrderBookData()).willReturn(orderBookDto);


        mvc.perform(get("/orderbook")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getTradingDataTest() throws Exception {
        TradingDataDto tradingDataDto = new TradingDataDto();
        tradingDataDto.setCurrency(TEST_CURRENCY);
        given(chartController.getTradingData()).willReturn(tradingDataDto);


        mvc.perform(get("/trading")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}