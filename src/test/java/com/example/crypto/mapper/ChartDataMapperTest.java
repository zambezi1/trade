package com.example.crypto.mapper;

import com.example.crypto.dto.LimitOrderDto;
import com.example.crypto.dto.OrderBookDto;
import com.example.crypto.dto.TradingDataDto;
import com.example.crypto.dto.TradingHourlyDataDto;
import com.example.crypto.utils.TradeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trade;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.temporal.TemporalUnit;
import java.util.*;

import static java.time.temporal.ChronoUnit.HOURS;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChartDataMapperTest {

    private static final String TEST_CURRENCY = "TEST_CUR";
    private static final String TEST_DATE = "12:50:44";
    private static final String TEST_PRICE = "20";
    private static final String TEST_AMOUNT = "0.3";
    private static final BigDecimal TEXT_MAX_V = new BigDecimal(10);
    private static final BigDecimal TEXT_MIN_V = new BigDecimal(9);
    private static final BigDecimal TEXT_VOLUME_V = new BigDecimal(5000);
    private static final BigDecimal TEST_PRICE_1 = new BigDecimal(20);
    private static final BigDecimal TEST_AMOUNT_1 = new BigDecimal(0.3);
    private static final BigDecimal TEST_PRICE_2 = new BigDecimal(3);
    private static final BigDecimal TEST_AMOUNT_2 = new BigDecimal(0.44444);
    private static final BigDecimal TEST_PRICE_3 = new BigDecimal(63);
    private static final BigDecimal TEST_AMOUNT_3 = new BigDecimal(2);
    private static final Date TEST_DATE_1 = new Date();
    private static final Date TEST_DATE_2 = lessDate(TEST_DATE_1);


    @Mock
    private Date dateBook;

    @Mock
    private BigDecimal amount;

    @Mock
    private BigDecimal price;

    @Spy
    private ChartDataMapper chartDataMapper = new ChartDataMapper();

    @Before
    public void init() {
        when(dateBook.toString()).thenReturn(TEST_DATE);
        when(price.toString()).thenReturn(TEST_PRICE);
        when(amount.toString()).thenReturn(TEST_AMOUNT);
    }

    @Test
    public void mapOrderBookTest() {
        LimitOrder bidEntry = mock(LimitOrder.class);
        when(bidEntry.getLimitPrice()).thenReturn(price);
        when(bidEntry.getOriginalAmount()).thenReturn(amount);
        List<LimitOrder> bids = Collections.singletonList(bidEntry);

        LimitOrder askEntry = mock(LimitOrder.class);
        when(askEntry.getLimitPrice()).thenReturn(price);
        when(askEntry.getOriginalAmount()).thenReturn(amount);
        List<LimitOrder> asks = Collections.singletonList(askEntry);

        OrderBook orderBook = new OrderBook(dateBook, asks, bids);

        Optional<OrderBook> orderBookOptional = Optional.of(orderBook);

        OrderBookDto orderBookDto = chartDataMapper.mapOrderBook(orderBookOptional, TEST_CURRENCY);
        assertFalse(orderBookDto.getAsks().isEmpty());
        assertFalse(orderBookDto.getBids().isEmpty());
        assertEquals(TEST_DATE, orderBookDto.getDate());
        assertEquals(TEST_CURRENCY, orderBookDto.getCurrency());

        LimitOrderDto bidDtoEntry = orderBookDto.getBids().get(0);
        assertEquals(TEST_PRICE, bidDtoEntry.getPrice());
        assertEquals(TEST_AMOUNT, bidDtoEntry.getAmount());

        LimitOrderDto askDtoEntry = orderBookDto.getAsks().get(0);
        assertEquals(TEST_PRICE, askDtoEntry.getPrice());
        assertEquals(TEST_AMOUNT, askDtoEntry.getAmount());
    }

    @Test
    public void mapTradingDataTest() {
        Trade trade1 = new Trade.Builder().timestamp(TEST_DATE_1).price(TEST_PRICE_1).originalAmount(TEST_AMOUNT_1).id("1").build();
        Trade trade2 = new Trade.Builder().timestamp(TEST_DATE_2).price(TEST_PRICE_2).originalAmount(TEST_AMOUNT_2).id("2").build();
        Trade trade3 = new Trade.Builder().timestamp(TEST_DATE_1).price(TEST_PRICE_3).originalAmount(TEST_AMOUNT_3).id("3").build();

        List<Trade> tradesListMock = new ArrayList<>();
        tradesListMock.add(trade1);
        tradesListMock.add(trade2);
        tradesListMock.add(trade3);

        Trades tradesMock = new Trades(tradesListMock);
        Optional<Trades> tradesOptional = Optional.of(tradesMock);

        Ticker tickerMock = new Ticker.Builder().high(TEXT_MAX_V).low(TEXT_MIN_V).volume(TEXT_VOLUME_V).build();
        Optional<Ticker> tickerOptional = Optional.of(tickerMock);

        TradingDataDto tradingDataDto = chartDataMapper.mapTradingData(tickerOptional, tradesOptional, TEST_CURRENCY);
        assertEquals(TEST_CURRENCY, tradingDataDto.getCurrency());
        assertEquals(TEXT_VOLUME_V.toString(), tradingDataDto.getVolume());
        assertNotNull(tradingDataDto.getVolatilityDay());


        assertFalse(tradingDataDto.getTradingHourlyData().isEmpty());

        TradingHourlyDataDto tradingHourlyDataDto1 = tradingDataDto.getTradingHourlyData().get(1);
        assertEquals(String.format("%d - %d", trade1.getTimestamp().getHours(), trade1.getTimestamp().getHours() + 1), tradingHourlyDataDto1.getHours());
        assertEquals(TradeUtils.getVolatility(TEST_PRICE_3, TEST_PRICE_1), tradingHourlyDataDto1.getVolatility());

        TradingHourlyDataDto tradingHourlyDataDto = tradingDataDto.getTradingHourlyData().get(0);
        assertEquals(String.format("%d - %d", trade2.getTimestamp().getHours(), trade2.getTimestamp().getHours() + 1), tradingHourlyDataDto.getHours());
        assertEquals(TradeUtils.getVolatility(TEST_PRICE_2, TEST_PRICE_2), tradingHourlyDataDto.getVolatility());
    }

    private static Date lessDate(Date date) {
        return Date.from(date.toInstant().minus(2, HOURS));
    }
}