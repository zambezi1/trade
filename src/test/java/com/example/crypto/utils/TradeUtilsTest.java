package com.example.crypto.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.dto.marketdata.Trade;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TradeUtilsTest {

    @Test
    public void getVolatilityTest() {
        assertEquals("0.9426551453", TradeUtils.getVolatility(new BigDecimal(7638), new BigDecimal(7710)));
    }

    @Test
    public void getTradesValotilityTest() {
        Trade trade1 = new Trade.Builder().price(new BigDecimal(10)).build();
        Trade trade2 = new Trade.Builder().price(new BigDecimal(1)).build();
        Trade trade3 = new Trade.Builder().price(new BigDecimal(2)).build();
        Trade trade4 = new Trade.Builder().price(new BigDecimal(3)).build();

        List<Trade> tradeList = new ArrayList<>();
        tradeList.add(trade1);
        tradeList.add(trade2);
        tradeList.add(trade4);
        tradeList.add(trade3);

        assertEquals("-90.0000000000", TradeUtils.getTradesValotility(tradeList));
    }
}