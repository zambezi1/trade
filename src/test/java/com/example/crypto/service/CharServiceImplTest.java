package com.example.crypto.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trade;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class CharServiceImplTest {

    private static final CurrencyPair TEST_CURRENCY = CurrencyPair.ADA_H18;

    @Mock
    private Exchange exchange;

    @Mock
    private MarketDataService marketDataService;

    @Mock
    private Date testDate;

    @Mock
    private BigDecimal max;

    @Mock
    private BigDecimal min;

    @Mock
    private BigDecimal volume;

    @InjectMocks
    private ChartServiceImpl charService;

    @Before
    public void init() {
        given(exchange.getMarketDataService()).willReturn(marketDataService);
    }

    @Test
    public void orderBookTest() throws IOException {
        List<LimitOrder> mockAsks = mock(List.class);
        List<LimitOrder> mockBids = mock(List.class);

        OrderBook orderBookMock = new OrderBook(testDate, mockAsks, mockBids);
        given(marketDataService.getOrderBook(TEST_CURRENCY)).willReturn(orderBookMock);

        Optional<OrderBook> orderBookOptional = charService.getOrderBook(TEST_CURRENCY);
        Assert.assertTrue(orderBookOptional.isPresent());

        OrderBook orderBook = orderBookOptional.get();
        Assert.assertEquals(testDate, orderBook.getTimeStamp());
        Assert.assertEquals(mockAsks, orderBook.getAsks());
        Assert.assertEquals(mockBids, orderBook.getBids());
    }

    @Test
    public void orderBookIoExceptionTest() throws IOException {
        given(marketDataService.getOrderBook(TEST_CURRENCY)).willThrow(new IOException());
        Optional<OrderBook> orderBookOptional = charService.getOrderBook(TEST_CURRENCY);
        Assert.assertFalse(orderBookOptional.isPresent());
    }

    @Test
    public void tickerTest() throws IOException {
        Ticker tickerMock = new Ticker.Builder().high(max).low(min).volume(volume).build();
        given(marketDataService.getTicker(TEST_CURRENCY)).willReturn(tickerMock);

        Optional<Ticker> tickerOptional = charService.getTicker(TEST_CURRENCY);
        Assert.assertTrue(tickerOptional.isPresent());

        Ticker ticker = tickerOptional.get();
        Assert.assertEquals(max, ticker.getHigh());
        Assert.assertEquals(min, ticker.getLow());
        Assert.assertEquals(volume, ticker.getVolume());
    }

    @Test
    public void tickerIoExceptionTest() throws IOException {
        given(marketDataService.getTicker(TEST_CURRENCY)).willThrow(new IOException());
        Optional<Ticker> tickerOptional = charService.getTicker(TEST_CURRENCY);
        Assert.assertFalse(tickerOptional.isPresent());
    }

    @Test
    public void tradesTest() throws IOException {
        List<Trade> tradesListMock = new ArrayList<>();
        Trades tradesMock = new Trades(tradesListMock);
        given(marketDataService.getTrades(TEST_CURRENCY, BitstampMarketDataServiceRaw.BitstampTime.DAY)).willReturn(tradesMock);

        Optional<Trades> tradesOptional = charService.getTradesForDay(TEST_CURRENCY);
        Assert.assertTrue(tradesOptional.isPresent());

        Trades trades = tradesOptional.get();
        Assert.assertEquals(tradesListMock, trades.getTrades());
    }

    @Test
    public void tradesIoExceptionTest() throws IOException {
        given(marketDataService.getTrades(TEST_CURRENCY, BitstampMarketDataServiceRaw.BitstampTime.DAY)).willThrow(new IOException());
        Optional<Trades> trades = charService.getTradesForDay(TEST_CURRENCY);
        Assert.assertFalse(trades.isPresent());
    }
}